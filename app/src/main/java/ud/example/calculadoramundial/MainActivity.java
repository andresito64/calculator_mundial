package ud.example.calculadoramundial;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText Valor01, Valor02;
    private RadioButton radioSuma;
    private RadioButton radioResta;
    private RadioButton radioMult;
    private RadioButton radioDiv;
    private Button Boton01;
    private TextView TextSal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Valor01 = (EditText) findViewById(R.id.editText01);
        Valor02 = (EditText) findViewById(R.id.editText02);
        radioSuma = (RadioButton) findViewById(R.id.radioSuma);
        radioResta = (RadioButton) findViewById(R.id.radioResta);
        radioMult = (RadioButton) findViewById(R.id.radioMult);
        radioDiv = (RadioButton) findViewById(R.id.radioDiv);
        Boton01 = (Button) findViewById(R.id.button);
        TextSal = (TextView) findViewById(R.id.textView2);
    }

    public void operation(View v){
        String text1 = Valor01.getText().toString();
        String text2 = Valor02.getText().toString();
        if (text1.isEmpty() || text2.isEmpty()){
            Toast.makeText(this, getResources().getText(R.string.message_empty), Toast.LENGTH_LONG).show();
            return;
        }
        Float number1 = Float.parseFloat(text1);
        Float number2 = Float.parseFloat(text2);
        int option = 0;
        if (radioSuma.isChecked()) option = 1;
        if (radioResta.isChecked()) option = 2;
        if (radioMult.isChecked()) option = 3;
        if (radioDiv.isChecked()) option = 4;
        switch (option){
            case 1:
                suma(number1, number2);
                break;
                case 2:
                    resta(number1, number2);
                break;
                case 3:
                    multi(number1, number2);
                break;
                case 4:
                    div(number1, number2);
                break;
            default:
                Toast.makeText(this, getResources().getText(R.string.select_option), Toast.LENGTH_LONG).show();
                break;

        }
    }

    private void suma(Float a, Float b){
        TextSal.setText(String.valueOf(a + b));
    }
    private void resta(Float a, Float b){
        TextSal.setText(String.valueOf(a - b));
    }
    private void multi(Float a, Float b){
        TextSal.setText(String.valueOf(a * b));
    }
    private void div(Float a, Float b){
        TextSal.setText(String.valueOf(a / b));
    }


}